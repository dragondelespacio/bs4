$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $(".carousel").carousel({
        interval: 2000
    });

    $("#contacto").on("show.bs.modal", function (e) {
        console.log("el modal esta empezando a abrir");
        $("#contactoBtn").removeClass("btn-outline-success");
        $("#contactoBtn").addClass("btn-primary");
        $("#contactoBtn").prop("disabled", true);
    });
    $("#contacto").on("shown.bs.modal", function (e) {
        console.log("modal esta completamente cargado");
    });
    $("#contacto").on("hide.bs.modal", function (e) {
        console.log("el modal se esta cerrando");
    });
    $("#contacto").on("hidden.bs.modal", function (e) {
        console.log("el modal se cerró");
        $("#contactoBtn").removeClass("btn-primary");
        $("#contactoBtn").addClass("btn-outline-success");
        $("#contactoBtn").prop("disabled", false);
    });
});